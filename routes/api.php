<?php

use App\Http\Resources\ProductResource;
use App\Http\Resources\UserCollection;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Resources\ProductCollection;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

 

Route::get('/users', function () {
    return new UserCollection(User::all());
});

// listar todos los productos 
Route::get('/product',[ProductController::class,'listProducts']);


// listar un producto por id
Route::get('/product/{id}',[ ProductController::class, 'listProductForId']);

// crear un producto nuevo
Route::post('/product', [ProductController::class, 'store']);

// eliminar un producto nuevo
Route::delete('/product/{id}',[ProductController::class,'deleteProduct']);


// modifico un parametro 
Route::put('/product',[ProductController::class,'modifyProduct']);

// crear un nuevo usuario
Route::post('/user',[UserController::class,'newUser']);

// listar usuarios
Route::get('/user',[UserController::class,'listUser']);

// actualizar usuario
Route::put('/user',[UserController::class,'modifyUser']);

// eliminar un usuario
Route::delete('/user',[UserController::class,'deleteUser']);