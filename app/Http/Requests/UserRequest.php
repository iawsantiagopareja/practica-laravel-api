<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'no hay nombre',
            'email.required' => 'no hay correo electronico',
            'email.unique' => 'el correo ya existe',
            'email.email' => 'email no valido',
            'password.required' => 'hay q colocar una contraseña'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'error'  => $validator->errors()
        ]));
    }


}
