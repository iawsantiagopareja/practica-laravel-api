<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function newUser(UserRequest $request) {
        $request['password'] = Hash::make($request->password);
        $user = User::create($request->all());
        return new UserResource($user);
    }

    public function listUser() {
        return new UserCollection(User::all());
    }


    public function modifyUser(Request $request) {
        $id = $request->id;
        $user = User::find($id);
        // verifico si el usuario existe 
        if (!$user) {
            return response()->json(["error" =>'usuario no existe'],200);
        }
        // veirifico si hay que cambiar la conraseña
        if ($request->passoword) {
            // encripto la contraseña 
            $request['password'] = Hash::make($request->password);
        }
        $user->update($request->all());
        return new UserResource($user);
    }


     
       public function deleteUser(Request $request) {
        $id = $request->id;
        $user = User::find($id);
        $mensaje = ["error" => "usuario no existe"];
        // si el producto existe lo elimino 
        if ($user) {
            $user->delete();
            $mensaje = ["success" => "usuario eliminado"];
        }
        return response()->json($mensaje);
    }

}
